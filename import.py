import cv2
import os
import random
import numpy as np
from matplotlib import pyplot as plt

#import tensorflow dependencies
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Layer, Conv2D, Dense, MaxPooling2D, Input, Flatten
#Model(input=[inputimage, verificationimage] ,output=[1,0])
#class L1Dist(Layer) 
#avoid oom errors by setting gpu memory consumption growth
gpus= tf.config.experimental.list_logical_devices('GPU')
for gpu in gpus :
    tf.config.experimental.set_memory_growth(gpu,True)
print(gpus)
for gpu in gpus:
    print(gpu)
print(len(gpus))

# Setup paths
POS_PATH = os.path.join('data', 'positive')
NEG_PATH = os.path.join('data', 'negative')
ANC_PATH = os.path.join('data', 'anchor')


# Make the directories
#os.makedirs(POS_PATH)
#os.makedirs(NEG_PATH)
#os.makedirs(ANC_PATH)

# Move LFW Images to the following repository data/negative
#for directory in os.listdir('lfw'):
  #  for file in os.listdir(os.path.join('lfw', directory)):
    #    EX_PATH = os.path.join('lfw', directory, file)
     #   NEW_PATH = os.path.join(NEG_PATH, file)
      #  os.replace(EX_PATH, NEW_PATH)

"""
# Import uuid library to generate unique image names
import uuid

os.path.join(ANC_PATH, '{}.jpg'.format(uuid.uuid1()))
# Establish a connection to the webcam
cap = cv2.VideoCapture(0)
while cap.isOpened():
    ret, frame = cap.read()

    # Cut down frame to 250x250px
    frame = frame[120:120 + 250, 200:200 + 250, :]

    # Collect anchors
    if cv2.waitKey(1) & 0XFF == ord('a'):
        # Create the unique file path
        imgname = os.path.join(ANC_PATH, '{}.jpg'.format(uuid.uuid1()))
        # Write out anchor image
        cv2.imwrite(imgname, frame)

    # Collect positives
    if cv2.waitKey(1) & 0XFF == ord('p'):
        # Create the unique file path
        imgname = os.path.join(POS_PATH, '{}.jpg'.format(uuid.uuid1()))
        # Write out positive image
        cv2.imwrite(imgname, frame)

    # Show image back to screen
    cv2.imshow('Image Collection', frame)

    # Breaking gracefully
    if cv2.waitKey(1) & 0XFF == ord('q'):
        break

# Release the webcam
cap.release()
# Close the image show frame
cv2.destroyAllWindows()

"""
#def data_aug(img):
 #   data = []
  #  for i in range(9):
   #     img = tf.image.stateless_random_brightness(img, max_delta=0.02, seed=(1, 2))
    #    img = tf.image.stateless_random_contrast(img, lower=0.6, upper=1, seed=(1, 3))
        # img = tf.image.stateless_random_crop(img, size=(20,20,3), seed=(1,2))
     #   img = tf.image.stateless_random_flip_left_right(img, seed=(np.random.randint(100), np.random.randint(100)))
      #  img = tf.image.stateless_random_jpeg_quality(img, min_jpeg_quality=90, max_jpeg_quality=100,
       #                                              seed=(np.random.randint(100), np.random.randint(100)))
        #img = tf.image.stateless_random_saturation(img, lower=0.9, upper=1,
         #                                          seed=(np.random.randint(100), np.random.randint(100)))
#
 #       data.append(img)

  #  return data
#img_path = os.path.join(ANC_PATH, '7ebc8d9e-d359-11ed-b834-c85acfdecd15.jpg')
#img = cv2.imread(img_path)
#augmented_images = data_aug(img)

#for image in augmented_images:
  #  cv2.imwrite(os.path.join(ANC_PATH, '{}.jpg'.format(uuid.uuid1())), image.numpy())

#for file_name in os.listdir(os.path.join(POS_PATH)):
  #  img_path = os.path.join(POS_PATH, file_name)
  #  img = cv2.imread(img_path)
   # augmented_images = data_aug(img)

   # for image in augmented_images:
     #   cv2.imwrite(os.path.join(POS_PATH, '{}.jpg'.format(uuid.uuid1())), image.numpy())

# Get Image Directories
anchor = tf.data.Dataset.list_files(ANC_PATH+'\*.jpg').take(350)
positive = tf.data.Dataset.list_files(POS_PATH+'\*.jpg').take(350)
negative = tf.data.Dataset.list_files(NEG_PATH+'\*.jpg').take(350)

dir_test = anchor.as_numpy_iterator()

print(dir_test.next())
#preprocessing
def preprocess(file_path):
    # Read in image from file path
    byte_img = tf.io.read_file(file_path)
    # Load in the image
    img = tf.io.decode_jpeg(byte_img)

    # Preprocessing steps - resizing the image to be 100x100x3
    img = tf.image.resize(img, (100, 100))
    # Scale image to be between 0 and 1
    img = img / 255.0

    # Return image
    return img
img = preprocess('data\\anchor\\64a3791f-d337-11ed-85c2-c85acfdecd15.jpg')
print(img.numpy().max())
plt.imshow(img)
plt.show()

#Create Labelled Dataset

positives = tf.data.Dataset.zip((anchor, positive, tf.data.Dataset.from_tensor_slices(tf.ones(len(anchor)))))
negatives = tf.data.Dataset.zip((anchor, negative, tf.data.Dataset.from_tensor_slices(tf.zeros(len(anchor)))))
data = positives.concatenate(negatives)
data
print(data)
print(len(anchor))

samples = data.as_numpy_iterator()
example = samples.next()
print(example)
 #Build Train and Test Partition
def preprocess_twin(input_img, validation_img, label):
    return(preprocess(input_img), preprocess(validation_img), label)

res = preprocess_twin(*example)
plt.imshow(res[1])
plt.show()
print(res[2])

# Build dataloader pipeline
data = data.map(preprocess_twin)
data = data.cache()
data = data.shuffle(buffer_size=10000)
# Training partition
train_data = data.take(round(len(data)*.7))
train_data = train_data.batch(16)
train_data = train_data.prefetch(8)
# Testing partition
test_data = data.skip(round(len(data)*.7))
test_data = test_data.take(round(len(data)*.3))
test_data = test_data.batch(16)
test_data = test_data.prefetch(8)


#Model Engineering
 #Build Embedding Layer
inp = Input(shape=(100,100,3), name='input_image')
c1 = Conv2D(64, (10,10), activation='relu')(inp)
m1 = MaxPooling2D(64, (2,2), padding='same')(c1)
c2 = Conv2D(128, (7,7), activation='relu')(m1)
m2 = MaxPooling2D(64, (2,2), padding='same')(c2)
c3 = Conv2D(128, (4,4), activation='relu')(m2)
m3 = MaxPooling2D(64, (2,2), padding='same')(c3)
c4 = Conv2D(256, (4,4), activation='relu')(m3)
f1 = Flatten()(c4)
d1 = Dense(4096, activation='sigmoid')(f1)
mod = Model(inputs=[inp], outputs=[d1], name='embedding')
mod.summary()

def make_embedding(): 
    inp = Input(shape=(100,100,3), name='input_image')

    # First block
    c1 = Conv2D(64, (10,10), activation='relu')(inp)
    m1 = MaxPooling2D(64, (2,2), padding='same')(c1)

    # Second block
    c2 = Conv2D(128, (7,7), activation='relu')(m1)
    m2 = MaxPooling2D(64, (2,2), padding='same')(c2)

    # Third block 
    c3 = Conv2D(128, (4,4), activation='relu')(m2)
    m3 = MaxPooling2D(64, (2,2), padding='same')(c3)

    # Final embedding block
    c4 = Conv2D(256, (4,4), activation='relu')(m3)
    f1 = Flatten()(c4)
    d1 = Dense(4096, activation='sigmoid')(f1)


    return Model(inputs=[inp], outputs=[d1], name='embedding')
embedding = make_embedding()
embedding.summary()

# Build Distance Layer
# Siamese L1 Distance class
class L1Dist(Layer):
    
    # Init method - inheritance
    def __init__(self, **kwargs):
        super().__init__()
       
    # Magic happens here - similarity calculation
    def call(self, input_embedding, validation_embedding):
        return tf.math.abs(input_embedding - validation_embedding)
l1 = L1Dist()
print(l1)
#l1(anchor_embedding, validation_embedding)